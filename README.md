Role "Workstation"
=========

Used to setup newly provisioned local workstation running fedora 32

Molecule test: [![pipeline status](https://gitlab.com/ansible-achadwick/roles/workstation/badges/master/pipeline.svg)](https://gitlab.com/ansible-achadwick/roles/workstation/-/commits/master)

Example Playbook
----------------

```
- name: Setup personal workstation
  hosts: localhost
  connection: local
  become: true
  
  roles:
    - workstation
```

Include this role using an Ansible Galaxy requirements.yml file. E.g.:
```
- src: git+https://gitlab.com/ansible-achadwick/roles/workstation.git
  scm: git
  version: "0.1.6"  # quoted, so YAML doesn't parse this as a floating-point value
  name: workstation
```
And running:
```
ansible-galaxy role install -r requirements.yml
```

License
-------

BSD
